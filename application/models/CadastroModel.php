<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
    include APPPATH.'libraries/cadastro/DadosPessoaisValidator.php';
    include APPPATH.'libraries/cadastro/Pessoa.php';
    include APPPATH.'libraries/cadastro/Endereco.php';
    include APPPATH.'libraries/cadastro/RedesSociais.php';
    include APPPATH.'libraries/cadastro/EnderecoValidator.php';
    include APPPATH.'libraries/cadastro/RedesSociaisValidator.php';


    class CadastroModel extends CI_Model{
        
        public function salvar(){
            if(sizeof($_POST) == 0) return;

            $dados = new DadosPessoaisValidator();
            $redes = new RedesSociaisValidator();
            $end = new EnderecoValidator();

            $dados->validate();
            $redes->validate();
            $end->validate();


            if($this->form_validation->run()){

                $pessoa = new Pessoa();
                //este V é um Vetor
                $v = $dados->getData();
                $id = $pessoa->salvar($v);

                $endereco = new Endereco();
                //este U é um vetor
                $u = $end->getData();
                $endereco->salvar($u,$id);


                //variavel rsoc é redes sociais
                $rsoc = new RedesSociais();
                $x = $redes->getData();
                $rsoc->salvar($x, $id);

            }            
            
           
           


        }
    }
?>