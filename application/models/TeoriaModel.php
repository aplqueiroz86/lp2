<?php

defined('BASEPATH') OR exit('no direct script access allowed');
//include_once APPPATH.'libraries/teoria/Pessoa.php';
include_once APPPATH.'libraries/teoria/Aluno.php';
include_once APPPATH.'libraries/teoria/Professor.php';
include_once APPPATH.'libraries/teoria/Funcionario.php';
include_once APPPATH.'libraries/teoria/Biblioteca.php';

class TeoriaModel extends CI_Model{
    
    public function heranca(){
        $p1 = new Pessoa('Maria', 25);
        $p2 = new Pessoa('Jose' , 28);

        echo '<br>';
        var_dump($p1); echo '<br>';
        var_dump($p2); echo '<br>';

        echo '<br>';
        echo " A idade de ".$p1->getNome()." é: ".$p1->getIdade();
        echo '<br>';
        echo " A idade de ".$p2->getNome()." é: ".$p2->getIdade();
        $p1->setIdade(34);
        $p2->setIdade(37);

        echo '<br><br><br>';
        echo '<h3><b>Depois de usar o SET:</b></h3>';
        echo " A idade de ".$p1->getNome()." é: ".$p1->getIdade();
        echo '<br>';
        echo " A idade de ".$p2->getNome()." é: ".$p2->getIdade();

        echo '<br><br><br>';
        echo '<h3><b>Obtendo a pressao:</b></h3>';
        echo " A pressao de ".$p1->getNome()." é: ".$p1->getPressao();
        echo '<br>';
        echo " A pressao de ".$p2->getNome()." é: ".$p2->getPressao();

        echo '<br><br><br>';
        echo '<h3><b>Obtendo a posição atual:</b></h3>';
        echo " A posicao atual de ".$p1->getNome()." é: ".$p1->andar();
        echo '<br>';
        echo " A posição atual de ".$p2->getNome()." é: ".$p2->andar();
        

        $a1 = new Aluno('Esther', 18);
        //var_dump($a1);
        $a1->setTurma('LP2 - Sexta');

        echo '<br><br><br>';
        echo '<h3><b>Metodos do ALUNO:</b></h3>';
        echo " A idade de ".$a1->getNome()." é: ".$a1->getIdade();
        echo '<br>';
        echo " A posição atual de ".$a1->getNome()." é: ".$p2->andar();
        echo '<br>';
        echo " A pressao de ".$a1->getNome()." é: ".$a1->getPressao();
        echo '<br>';
        echo " A pressao de ".$a1->getNome()." é: ".$a1->getTurma();
        
    }



    public function polimorfismo(){

        $livros = array(2,8,6,3,9,5);
        $dias= 30;
        $aluno = new Aluno('Maria',25,'ADS');
        $prof = new Professor('João',45,'IFSP');
        $func = new Funcionario('José',35,'Carpintaria');

    



        $bib = new Biblioteca();
        $bib->emprestimo($livros,$aluno);
        $bib->emprestimo($livros,$prof);
        $bib->emprestimo($livros,$func);


        $bib->devolucao($dias,$livros,$aluno);
        $bib->devolucao($dias,$livros,$prof);
        $bib->devolucao($dias,$livros,$func);




    }

}