<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
    include APPPATH.'libraries/ana/PesquisaValidator.php';
    include APPPATH.'libraries/ana/Pesquisa.php';
    include APPPATH.'libraries/ana/InformaAPI.php';
    
    class AnaModel extends CI_Model{
        
        public function enviar(){
            if(sizeof($_POST) == 0) return;

            $dados = new PesquisaValidator();
           
            $dados->validate();          


            if($this->form_validation->run()){

                $pesquisaFeita = new Pesquisa($dados);
                $nyt = $dados->getData();
                $pesquisa = $pesquisaFeita->enviar($nyt);

                
            }            
          
        }


        public function infoAPI(){
            $api = new InformaAPI();
        }
    }
?>