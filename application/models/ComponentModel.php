<?php
defined('BASEPATH') OR exit('no direct script access allowed');
include APPPATH. 'libraries/component/Table.php';
include APPPATH. 'libraries/component/Panel.php';
include_once APPPATH.'libraries/User.php';

class ComponentModel extends CI_Model{
    
    private $color = array('primary','secondary','sucess','warning','danger','info','dark','light');

    public function getPanelList(){
        $rs = $this->db->get('panel_data',9);
        $v = $rs->result();
        $html = '';

        foreach($v as  $row){
            $panel = new Panel($row);
            $panel->setCols(4);
            $panel->setColor($this->color[rand(0,7)]);
            $html .= $panel->getHtml();
        }
        return $html;
    }

    public function getTable(){
        $user = new User;
        $data =  $user->getAll();
        $header = array('','Nome','Sobrenome','Email','Telefone','Senha','Criado em:');

        $table = new Table($data,$header);
        $table->set_header_color('purple-gradient');
        $table->use_white_text();
        //$table->zebra_table();
        //$table->use_border();
        $table->use_hover();
        $table->use_action_button();
        //$table->small_table();
        $table->column_size(8);
        //$table->align_left();
        $table->mt(3);


        return $table->getHTML();
    }

  }
?>