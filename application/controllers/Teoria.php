<?php
defined('BASEPATH') OR exit('no direct script access allowed');

class Teoria extends CI_Controller{
    public function index(){
        echo "Um pouco da teoria relacionada a POO";
    }

    public function heranca(){
        echo "1. O conteudo de uma classe";
        $this->load->model('TeoriaModel', 'teoria'); //teoria é apelido de TeoriaModel
        $this->teoria->heranca();
    }


    public function polimorfismo(){
        
        $this->load->model('TeoriaModel', 'teoria'); 
        $this->teoria->polimorfismo();
    }


}