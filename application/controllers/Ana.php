<?php

defined('BASEPATH') OR exit ('No direct script acess allowed');

    class Ana extends MY_Controller{
        
        public function index(){
            $this->load->model('AnaModel', 'model');
            
            $this->model->enviar();

            $html = $this->load->view('ana/index', null, true);
            $this->show($html);
        }


        public function pesquisa(){
            $this->load->model('AnaModel', 'model');
            
            $this->model->enviar();

            $html = $this->load->view('ana/form_pesquisa', null, true);
            $this->show($html);
        }



        public function infoAPI(){
            $this->load->model('AnaModel', 'model');
            
            $html = $this->load->view('ana/informa_api', null, true);
            $this->show($html);
        }

    
    }

 
