<div class="card" id="infoAPI">
  <div class="card-header">
    Informações sobre a Movie Reviews API
  </div>
  <div class="card-body">
    <blockquote class="blockquote mb-0">
      <p>Quais são as principais características da API e suas finalidades?</p>
      <footer class="blockquote-footer">Esta API tem a finalidade de ser uma fonte de informações sobre filmes em geral.  </footer>
    </blockquote>
    <blockquote class="blockquote mb-0">
      <p>O quê é possível criar com o uso desta API?</p>
      <footer class="blockquote-footer">É possível criar a busca de críticas ou resenhas, links e multimidia dos filmes. </footer>
    </blockquote>
    <blockquote class="blockquote mb-0">
      <p>Quais são as restrições para o uso da API? Tem custo? Tem boa documentação? E outras...</p>
      <footer class="blockquote-footer">Para usar a API é necessário solicitar uma senha, que é usada como parametro de pesquisa. Há também um manual de uso de marca (logo) do New York Times que é solicitado para ser usado em qualquer aplicativo que utilizarem a API, para não ser utilizado deve ter uma autorização prévia do NYT, além do limite de acessos. A documentação desta API, chega a ser praticamente zero.</footer>
    </blockquote>
    <blockquote class="blockquote mb-0">
      <p>Quais foram os passos necessários para a implementação da sua aplicação com o uso desta API?</p>
      <footer class="blockquote-footer">Não consegui executar a API como deveria, infelizmente não foi concluida a aplicação. Um item que ajudou muito importante em dificultar a utilização de APIs é a falta de documentação. </footer>
    </blockquote>
  </div>
</div>






