<div class="container">
    
    <?= validation_errors('<div class="alert alert-danger">', '</div>'); ?>

    <div class="card">
        <div class="card-header">
            <h4>Pesquisar Filmes </h4>
        </div>
        <div class="card-body">
            <form method="POST" class="text-center border border-light p-5">

                <p class="h4 mb-4">Pesquise</p>

                <div class="form-row mb-4">
                    <div class="col">
                        <input type="text" name="pesquisa" value="<?= set_value('pesquisa')?>" class="form-control" placeholder="Digite sua pesquisa...">
                    </div>                    
                </div>                
        </div>
    </div>
    <button class="btn btn-info my-4 btn-block" type="submit">Enviar</button>

    
</div>