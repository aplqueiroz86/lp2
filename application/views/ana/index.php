<ul class="nav nav-tabs nav-justified md-tabs teal lighten-5" id="myTabJust" role="tablist">
  <li class="nav-item">
    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home-just"
      aria-selected="true">Home</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="utilizarAPI-tab" data-toggle="tab" href="#utilizarAPI" role="tab" aria-controls="profile-just"
      aria-selected="false">Usar API</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="informaAPI-tab" data-toggle="tab" href="#informaAPI" role="tab" aria-controls="contact-just"
      aria-selected="false">Informações da API</a>
  </li>
</ul>


<div class="tab-content card pt-5" id="myTabContentJust">
  <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">


  <div class="media">
  <img class="d-flex mr-3" src="https://suap.ifsp.edu.br/media/alunos/150x200/156414.MjPaGM2fonb7.jpg">
  <div class="media-body">
    <h4 class="mt-0 font-weight-bold">Aluna: Ana Paula de Lima Queiroz</h4>
    <p>Matéria: LPII</p>
    <p>Turma: B (Sexta-feira)</p>
    <p>RA: GU300287-X</p>

    <div class="media mt-4">
      <a class="d-flex pr-3" href="https://developer.nytimes.com/apis">
        <img src="https://developer.nytimes.com/files/poweredby_nytimes_200c.png?v=1539041473000">
      </a>
      <div class="media-body">
        <h4 class="mt-0 font-weight-bold">Movie Reviews API</h4>
        <p>API destinada a informações de criticas, resenhas, e informações sobre Filmes publicados no New York Times </p>
      </div>
    </div>
  </div>
</div>





    
  
  </div>
  <div class="tab-pane fade" id="utilizarAPI" role="tabpanel" aria-labelledby="utilizarAPI-tab">
   
  <div class="container">
    
    <?= validation_errors('<div class="alert alert-danger">', '</div>'); ?>

    <div class="card">
        <div class="card-header">
            <h4>Pesquisar Filmes </h4>
        </div>
        <div class="card-body">
            <form method="POST" class="text-center border border-light p-5">

                <p class="h4 mb-4">Pesquise</p>

                <div class="form-row mb-4">
                    <div class="col">
                        <input type="text" name="pesquisa" value="<?= set_value('pesquisa')?>" class="form-control" placeholder="Digite sua pesquisa...">
                    </div>                    
                </div>                
        </div>
    </div>
    <button class="btn btn-info my-4 btn-block" type="submit">Enviar</button>
       
</div>


  </div>





  <div class="tab-pane fade" id="informaAPI" role="tabpanel" aria-labelledby="informaAPI-tab">
   
<div class="card" id="infoAPI">
  <div class="card-header">
    Informações sobre a Movie Reviews API
  </div>
  <div class="card-body">
    <blockquote class="blockquote mb-0">
      <p>Quais são as principais características da API e suas finalidades?</p>
      <footer class="blockquote-footer">Esta API tem a finalidade de ser uma fonte de informações sobre filmes em geral.  </footer>
    </blockquote>
    <blockquote class="blockquote mb-0">
      <p>O quê é possível criar com o uso desta API?</p>
      <footer class="blockquote-footer">É possível criar a busca de críticas ou resenhas, links e multimidia dos filmes. </footer>
    </blockquote>
    <blockquote class="blockquote mb-0">
      <p>Quais são as restrições para o uso da API? Tem custo? Tem boa documentação? E outras...</p>
      <footer class="blockquote-footer">Para usar a API é necessário solicitar uma senha, que é usada como parametro de pesquisa. Há também um manual de uso de marca (logo) do New York Times que é solicitado para ser usado em qualquer aplicativo que utilizarem a API, para não ser utilizado deve ter uma autorização prévia do NYT, além do limite de acessos. A documentação desta API, chega a ser praticamente zero.</footer>
    </blockquote>
    <blockquote class="blockquote mb-0">
      <p>Quais foram os passos necessários para a implementação da sua aplicação com o uso desta API?</p>
      <footer class="blockquote-footer">Não consegui executar a API como deveria, infelizmente não foi concluida a aplicação. Um item que ajudou muito importante em dificultar a utilização de APIs é a falta de documentação. </footer>
    </blockquote>
  </div>
</div>




  </div>
</div>