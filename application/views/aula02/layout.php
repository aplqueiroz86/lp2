<div class="container">
    <div class="row">
        <div class="col-md-12 mt-4 mb-2">
            <h2 class="text-center">Cadastro dos clientes 2019</h2>
        </div>
    </div>
    
    <div class="row justify-content-between">
        <div class="col-md-7">
            <?= $formulario ?>
        </div>
        <div class="col-md-4 mt-5">
            <?= $cartao ?>
        </div>
    </div>
        
        <div class="row">
            <div class="col-md-12">
                <p class="text-center red-text bold font-weight-bold">Cuidado com o preenchimento correto do formulario</p>
            </div>
        </div>
</div>

<div class="row">
    <div class="col-md-12">
        <?= $jumbotron ?>
    </div>
</div>

<div class="row">
    <div class="col-sm-6 col-md-4 col-lg-3 col-xl-2"><?= $image ?></div>
    <div class="col-sm-6 col-md-4 col-lg-3 col-xl-2"><?= $image ?></div>
    <div class="col-sm-6 col-md-4 col-lg-3 col-xl-2"><?= $image ?></div>
    <div class="col-sm-6 col-md-4 col-lg-3 col-xl-2"><?= $image ?></div>
    <div class="col-sm-6 col-md-4 col-lg-3 col-xl-2"><?= $image ?></div>
    <div class="col-sm-6 col-md-4 col-lg-3 col-xl-2"><?= $image ?></div>
</div>