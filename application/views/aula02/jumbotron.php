<div class="jumbotron text-center hoverable p-4">
  <div class="row">
    <div class="col-md-4 offset-md-1 mx-3 my-3">
      <div class="view overlay">
        <img src="https://mdbootstrap.com/img/Photos/Others/images/<?= $img ?>.jpg" class="img-fluid" alt="Sample image for first version of blog listing">
        <a>
          <div class="mask rgba-white-slight"></div>
        </a>
      </div>
    </div>
    <div class="col-md-7 text-md-left ml-3 mt-3">
      <a href="#!" class="blue-text">
        <h6 class="h6 pb-1"><i class="fas fa-moon pr-1"></i> <?= $detail ?></h6>
      </a>
      <h4 class="h4 mb-4"><?= $title ?></h4>
      <p class="font-weight-normal"><?= $content ?></p>
      <p class="font-weight-normal">Autor(a):  <a><strong><?= $author ?></strong></a>, <?= $date ?></p>
      <a class="btn btn-dark"><?= $label ?></a>
    </div>
  </div>
</div>