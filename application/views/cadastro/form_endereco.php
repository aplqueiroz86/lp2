<div class="container mt-3">
    <div class="card">
        <div class="card-header">
            <h4>Endereço</h4>
        </div>
        <div class="card-body">
                <div class="form-row">
                    <div class="col-md-4">
                    <div class="md-form form-group">
                        <input type="text" value="<?= set_value('tipo_logradouro')?>" id="tipo_logradouro" name="tipo_logradouro" class="form-control" placeholder="Avenida, Rua, etc...">
                        <label for="tipo_logradouro">Tipo do Logradouro</label>
                    </div>
                    </div>

                    <div class="col-md-8">
                    <div class="md-form form-group">
                        <input type="text" value="<?= set_value('nome_logradouro')?>" class="form-control" id="nome_logradouro" name="nome_logradouro" placeholder="Tiradentes, Paulista...">
                        <label for="nome_logradouro">Nome Logradouro</label>
                    </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-2">
                        <div class="md-form form-group">
                            <input type="text" value="<?= set_value('numero')?>" class="form-control" id="numero" name="numero">
                            <label for="numero">Número</label>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="md-form form-group">
                            <input type="text" value="<?= set_value('complemento')?>" class="form-control" id="complemento" name="complemento">
                            <label for="complemento">Complemento</label>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="md-form form-group">
                            <input type="text" value="<?= set_value('cep')?>" class="form-control" id= "cep" name="cep" placeholder="00.000-000">
                            <label for="cep">Cep</label>
                        </div>
                    </div>
                </div>

                <div class="form-row">
                    <div class="col-md-6">
                    <div class="md-form form-group">
                        <input type="text" value="<?= set_value('cidade')?>" class="form-control" id="cidade" name="cidade" placeholder="São Paulo, Guarulhos...">
                        <label for="cidade" >Cidade</label>
                    </div>
                    </div>

                    <div class="col-md-6">
                    <div class="md-form form-group">
                        <input type="text" value="<?= set_value('estado')?>" class="form-control" id="estado" name="estado" placeholder="São Paulo, Rio de Janeiro...">
                        <label for="estado">Estado</label>
                    </div>
                    </div>
                </div>
                
    </div></div>
</div>


