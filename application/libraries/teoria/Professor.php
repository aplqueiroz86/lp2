<?php

include_once 'Pessoa.php'; 


class Professor extends Pessoa{

    private $escola;
    private $curso = array();

    function __construct($nome,$idade,$escola){

        parent::__construct($nome,$idade);
        $this->escola = $escola;
    }

    public function addCurso($curso){
        $this->curso[] = $curso;

    }

    public function lecionar(){
        echo 'apresentar conteudo de qualidade para seus alunos<br>';

    }

    public function pesquisar(){
        echo 'descobrir coisas novas para aprimorar seu trabalho<br>';

    }



    public function limiteLivros(){
        return 10;
    }

    public function prazoDeEntrega(){
        return 30;
    }

}