<?php

include_once 'Pessoa.php'; 

//Leia como: Aluno é pessoa
class Aluno extends Pessoa{
    private $turma;
    private $curso;

   
    public function __construct($nome,$idade,$turma){
        parent ::__construct($nome, $idade);
        $this->turma = $turma;
    }
   

    public function setTurma($turma){
        $this->turma = $turma;
    }

    public function getTurma(){
        return $this->turma;
    }

    public function estudar(){
        echo 'Passar horas passeando na Internet';
    }

    public function pesquisar(){
        echo 'Passar horas pesquisando na Internet';
    }

    public function limiteLivros(){
        return 7;
    }

    public function prazoDeEntrega(){
        return 21;
    }




}
