<?php
    defined('BASEPATH') OR exit('No direct script access allowed');

    include_once 'Validator.php';
    
        class PesquisaValidator extends Validator{

            public function validate(){
                $this->form_validation->set_rules('pesquisa', 'Pesquisa', 'required');
         
              
            }

            public function getData(){
                $data['pesquisa'] = $this->input->post('pesquisa');
         
                //Realizar ações corretivas 
                return $data;
            }

        }