<?php
    defined('BASEPATH') OR exit('No direct script access allowed');

    include_once 'Validator.php';
    
        class DadosPessoaisValidator extends Validator{

            public function validate(){
                $this->form_validation->set_rules('nome', 'Nome', 'required|min_length[3]|max_length[15]');
                $this->form_validation->set_rules('sobrenome', 'Sobrenome', 'required|min_length[2]|max_length[50]');
                $this->form_validation->set_rules('email', 'Email', 'required|is_unique[pessoa.email]|valid_email');
                $this->form_validation->set_rules('senha', 'Senha', 'required|min_length[8]');
                $this->form_validation->set_rules('nascimento', 'Nascimento', 'required|max_length[10]');

                
            }

            public function getData(){
                $data['nome']       = $this->input->post('nome');
                $data['sobrenome']  = $this->input->post('sobrenome');
                $data['email']      = $this->input->post('email');
                $data['senha']      = $this->input->post('senha');
                $data['nascimento'] = $this->input->post('nascimento');
                //Realizar ações corretivas 
                return $data;
            }

        }