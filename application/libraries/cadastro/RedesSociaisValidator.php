<?php
   // defined('BASEPATH') OR exit('No direct script access allowed');

   include_once 'Validator.php';
    
        class RedesSociaisValidator extends Validator{

            public function validate(){
                $this->form_validation->set_rules('facebook', 'Facebook', 'trim|required');
                $this->form_validation->set_rules('twitter', 'Twitter', 'trim|required');
                $this->form_validation->set_rules('instagram', 'Instagram', 'trim|required');
                $this->form_validation->set_rules('linkedin', 'Linkedin', 'trim|required');
              

                
            }

            public function getData(){
                $data['facebook']       = $this->input->post('facebook');
                $data['twitter']        = $this->input->post('twitter');
                $data['instagram']      = $this->input->post('instagram');
                $data['linkedin']       = $this->input->post('linkedin');
           
                //Realizar ações corretivas 
                return $data;
            }

        }